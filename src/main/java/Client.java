import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;


public class Client {

    public void getMessage(BufferedReader bufferedReader) throws IOException {
            System.out.println("Client got message: " + bufferedReader.readLine());
    }

    public void sendMessage(PrintStream printStream, String  message) throws IOException {
        printStream.println(message);
        printStream.flush();
    }

    public static void main(String[] args) {
        try(    Socket socket = new Socket(InetAddress.getLocalHost(), 8030);
                BufferedReader   bufferedReader =  new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintStream printStream = new PrintStream(socket.getOutputStream());
                ){
            Client client = new Client();
            for(int i=0;i<10;i++){
            client.sendMessage(printStream,"Hi, server, message №" + i);
            Thread.sleep(500);
            client.getMessage(bufferedReader);
            }
        } catch (IOException e) {
            System.err.println("ошибка: " + e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

