import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private ServerSocket serverSocket;

    public static class SingletonHolder {
        public static final Server HOLDER_INSTANCE = new Server();
    }

    public static Server getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    public Server() {
        try {
            serverSocket = new ServerSocket(8030);
        } catch (IOException e) {
            System.err.println("Creating server error");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        try {
            Server server = Server.getInstance();
            System.out.println("Server initialized");
            while (true) {
                Socket client = server.serverSocket.accept();
                ServerThread serverThread = new ServerThread(client);
                serverThread.start();
            }
        } catch (IOException e) {
            System.err.println("Ошибка: " + e);
        }
    }
}
