import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class ServerThread extends Thread {
    private Socket clientSocket;

    public ServerThread(Socket clientSocket) throws IOException{
        this.clientSocket = clientSocket;

    }

    public void getMessage(String str) throws IOException {
        System.out.println("Server got message: " + str);
    }

    public void sendMessage(PrintStream ps, int messageNumber) throws IOException {
        ps.println("Hello client, message № " + messageNumber);
        ps.flush();
    }

    @Override
    public void run() {
        int i = 0;
        try (
                BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                PrintStream output = new PrintStream(clientSocket.getOutputStream())
        ) {
            String str;
            while((str = input.readLine()) != null) {
                getMessage(str);
                sendMessage(output, i++);
            }

        }catch (IOException e) {
            System.err.println("Ошибка: " + e);
        } finally {
            this.interrupt();
        }
    }
}
